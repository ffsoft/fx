<?php

namespace ffsoft\fx\controllers;

use ffsoft\fx\drivers\Cnb;
use ffsoft\fx\drivers\Kurzy;
use ffsoft\fx\models\FxRates;
use ffsoft\fx\services\RatesService;
use yii\console\Controller;

/**
 * Class RatesController
 *
 * @package ffsoft\fx\controllers
 */
class RatesController extends Controller
{
    /** @var string Get rate from this date. A date/time string. Valid formats are explained in Date and Time Formats. */
    public $from;
    /** @var string Get rate to this date. A date/time string. Valid formats are explained in Date and Time Formats. */
    public $to;

    /**
     * @inheritdoc
     */
    public function actions()
    {
        $actions['cnb'] = [
            'class'       => RatesService::class,
            'modelClass'  => FxRates::class,
            'sourceClass' => Cnb::class,
            'from'        => $this->from,
            'to'          => $this->to,
        ];
        $actions['kurzy'] = [
            'class'       => RatesService::class,
            'modelClass'  => FxRates::class,
            'sourceClass' => Kurzy::class,
        ];

        return $actions;
    }

    /**
     * @inheritdoc
     */
    public function optionAliases()
    {
        return ['f' => 'from', 't' => 'to'];
    }

    /**
     * @inheritdoc
     */
    public function options($actionID)
    {
        switch ($actionID) {
            case 'cnb':
                $options = ['from', 'to'];
                break;
            default:
                $options = [];
                break;
        }

        return array_merge(parent::options($actionID), $options);
    }
}
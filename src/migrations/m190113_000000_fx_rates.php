<?php

namespace ffsoft\fx\migrations;

use yii\db\Migration;

/**
 * Class m190113_000000_fx_rates
 *
 * @package ffsoft\fx\migrations
 */
class m190113_000000_fx_rates extends Migration
{
    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-fx_rates-currency_id_out-currencies-id', 'fx_rates');
        $this->dropForeignKey('fk-fx_rates-currency_id_in-currencies-id', 'fx_rates');
        $this->dropTable('{{%fx_rates}}');
    }

    /**
     * @return bool|void
     */
    public function safeUp()
    {

        $tableOptions = 'ENGINE = InnoDB ROW_FORMAT = DYNAMIC DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_unicode_ci';

        /**
         * Таблица FX курсов.
         * type enum FxRatesType (buy || sell)
         */
        $this->createTable('{{%fx_rates}}', [
            'id'              => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'currency_id_in'  => $this->integer(11)->notNull(),
            'currency_id_out' => $this->integer(11)->notNull(),
            'rate'            => $this->decimal(20, 10)->notNull(),
            'source'          => $this->integer(11)->notNull(),
            'type'            => $this->integer(11)->notNull(),
            'created_at'      => $this->integer(11)->null(),
            'updated_at'      => $this->integer(11)->null(),
            'deleted_at'      => $this->integer(11)->null(),
        ], $tableOptions);

        $this->addForeignKey('fk-fx_rates-currency_id_in-currencies-id', 'fx_rates',
            'currency_id_in', 'currencies', 'id', 'RESTRICT', 'RESTRICT');
        $this->addForeignKey('fk-fx_rates-currency_id_out-currencies-id', 'fx_rates',
            'currency_id_out', 'currencies', 'id', 'RESTRICT', 'RESTRICT');
    }
}
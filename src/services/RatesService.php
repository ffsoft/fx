<?php

namespace ffsoft\fx\services;

use common\models\Currencies;
use common\models\enums\RateTypes;
use DateInterval;
use DatePeriod;
use DateTime;
use ffsoft\fx\drivers\RatesInterface;
use ffsoft\fx\models\FxRates;
use Yii;
use yii\base\Action;
use yii\console\ExitCode;
use yii\helpers\ArrayHelper;

/**
 * Class RatesService
 *
 * @package ffsoft\fx\services
 */
class RatesService extends Action
{
    /** @var string Get rate from this date. A date/time string. Valid formats are explained in Date and Time Formats. */
    public $from;
    /** @var string Model class name */
    public $modelClass;
    /** @var string Source class name */
    public $sourceClass;
    /** @var string Get rate to this date. A date/time string. Valid formats are explained in Date and Time Formats. */
    public $to;
    /** @var RatesInterface */
    private $source;

    /**
     * @inheritdoc
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        $this->source = Yii::createObject($this->sourceClass);
        $historical = false;

        if (!empty($this->from)) {
            $from = strtotime($this->from);
            $historical = true;
        } else {
            $from = strtotime('01.01.2018');
        }

        if (!empty($this->to)) {
            $to = strtotime($this->to);
            $historical = true;
        } else {
            $to = time();
        }

        if ($historical) {
            $res = $this->getHistoricalRates($from, $to, 1);
        } else {
            $res = $this->getRates();
        }

        echo $res . PHP_EOL;
    }

    /**
     * @param int $from
     * @param int $to
     * @param int $interval
     *
     * @return int
     */
    private function getHistoricalRates(int $from, int $to, int $interval)
    {
        $currencies = Currencies::find()->isActive()->isFiat()->notDeleted()->all();
        $types = RateTypes::getConstantsByName();

        $period = $this->getPeriod($from, $to, $interval);
        if ($period === false) {
            return ExitCode::USAGE;
        }

        foreach ($period as $date) {
            foreach ($currencies as $currencyIn) {
                foreach ($currencies as $currencyOut) {
                    if ($currencyIn->getName() !== $currencyOut->getName()) {
                        foreach ($types as $type) {
                            $rates = FxRates::find()
                                ->select('created_at')
                                ->where([
                                    'currency_id_in'  => $currencyIn->getId(),
                                    'currency_id_out' => $currencyOut->getId(),
                                    'source'          => $this->source->getSource(),
                                    'type'            => $type,
                                ])
                                ->orderBy(['id' => SORT_DESC])
                                ->asArray()
                                ->cache(60)
                                ->all();

                            if (!ArrayHelper::isIn($date->getTimestamp(), $rates)) {
                                $fxRate = $this->source->getRate($currencyIn, $currencyOut, $type,
                                    $date->getTimestamp());

                                if ($fxRate instanceof FxRates && $fxRate->validate()) {
                                    if (!$fxRate->save(false)) {
                                        return ExitCode::IOERR;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return ExitCode::OK;
    }

    /**
     * @param int $from
     * @param int $to
     * @param int $interval
     *
     * @return \DateTimeInterface[]|\DatePeriod|bool
     */
    private function getPeriod(int $from, int $to, int $interval)
    {
        try {
            $begin = new DateTime('@' . $from);
            $end = new DateTime('@' . $to);
            $dateInterval = new DateInterval('P' . $interval . 'D');

            return new DatePeriod($begin, $dateInterval, $end);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return int
     */
    private function getRates()
    {
        $currencies = Currencies::find()->isActive()->isFiat()->notDeleted()->all();
        foreach ($currencies as $currencyIn) {
            foreach ($currencies as $currencyOut) {
                if ($currencyIn->getName() !== $currencyOut->getName()) {
                    $types = RateTypes::getConstantsByName();
                    foreach ($types as $type) {
                        $fxRate = $this->source->getRate($currencyIn, $currencyOut, $type, time());
                        if ($fxRate instanceof FxRates && $fxRate->validate()) {
                            if (!$fxRate->save(false)) {
                                return ExitCode::IOERR;
                            }
                        }
                    }
                }
            }
        }

        return ExitCode::OK;
    }
}
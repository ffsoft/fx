<?php

namespace ffsoft\fx\drivers;

use common\models\Currencies;
use common\models\enums\RateTypes;
use DateTime;
use ffsoft\fx\enums\FxRatesSource;
use ffsoft\fx\models\FxRates;
use Yii;
use yii\base\Exception;

/**
 * Class Cnb
 *
 * @package ffsoft\fx\drivers
 */
class Cnb implements RatesInterface
{
    /** Currency code */
    const DEFAULT_CURRENCY = 'CZK';
    /** @var string */
    private $url = 'https://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt';

    /**
     * @param Currencies $currencyIn
     * @param Currencies $currencyOut
     * @param int        $type
     * @param int        $timestamp
     *
     * @return FxRates|object|null
     * @throws \Exception
     */
    public function getRate(Currencies $currencyIn, Currencies $currencyOut, int $type, int $timestamp)
    {
        $date = date('d.m.Y', $timestamp);
        $key = [$date, __METHOD__];
        $cache = Yii::$app->cache->get($key);

        if ($cache === false) {
            $cache = $this->parseData($date);
            Yii::$app->cache->set($key, $cache, 60 * 10);
        }

        $rate = $this->fetchData($cache, $currencyIn->getName(), $currencyOut->getName(), $type);

        if (!empty($rate)) {
            $fxRate = Yii::createObject([
                'class'           => FxRates::class,
                'currency_id_in'  => $currencyIn->getId(),
                'currency_id_out' => $currencyOut->getId(),
                'source'          => $this->getSource(),
                'rate'            => $rate['rate'],
                'type'            => $rate['type'],
                'created_at'      => $rate['created_at'],
            ]);

            return $fxRate;
        } else {
            return null;
        }
    }

    /**
     * @return int
     */
    public function getSource()
    {
        return FxRatesSource::CNB;
    }

    /**
     * @param array  $data
     * @param string $currency_in
     * @param string $currency_out
     * @param int    $type
     *
     * @return array|null
     */
    private function fetchData(array $data, string $currency_in, string $currency_out, int $type)
    {
        foreach ($data as $rate) {
            if ($currency_in == $rate['currency_in']
                && $currency_out == $rate['currency_out']
                && $type == $rate['type']
            ) {
                return $rate;
            }
        }

        return null;
    }

    /**
     * @param array $params
     *
     * @return bool|string
     * @throws Exception
     */
    private function makeQuery(array $params)
    {
        $url = $this->url . '?' . http_build_query($params, '', '&');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        $response = curl_exec($ch);
        if ($response === false) {
            throw new Exception('Could not get reply: ' . curl_error($ch));
        }

        if (is_null($response)) {
            throw new Exception('Invalid data received, please make sure connection is working and requested API exists');
        }

        return $response;
    }

    /**
     * @param string $date
     *
     * @return array|false
     * @throws \Exception
     */
    private function parseData(string $date)
    {
        $response = $this->makeQuery(['date' => $date]);

        if ($response) {
            $date = strtok($response, ' ');
            $date = new DateTime($date);
            $timestamp = $date->getTimestamp();

            $rows = explode("\n", trim($response));
            /** Cleanup array */
            array_shift($rows);
            array_shift($rows);

            $data = [];
            foreach ($rows as $row) {
                $values = explode('|', $row);
                $currency = $values[3];
                $rate = floatval(str_replace(',', '.', $values[4]));
                $amount = $values[2];
                if (empty($amount)) {
                    continue;
                }

                $data[] = [
                    'currency_in'  => $currency,
                    'currency_out' => self::DEFAULT_CURRENCY,
                    'rate'         => round($rate / $amount, 8),
                    'type'         => RateTypes::SELL,
                    'created_at'   => $timestamp,
                ];

                $data[] = [
                    'currency_in'  => self::DEFAULT_CURRENCY,
                    'currency_out' => $currency,
                    'rate'         => round($rate / $amount, 8),
                    'type'         => RateTypes::BUY,
                    'created_at'   => $timestamp,
                ];
            }

            return $data;
        } else {
            return false;
        }
    }
}
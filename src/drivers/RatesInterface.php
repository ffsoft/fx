<?php

namespace ffsoft\fx\drivers;

use common\models\Currencies;
use ffsoft\fx\models\FxRates;

interface RatesInterface
{
    /**
     * @param Currencies $currencyIn
     * @param Currencies $currencyOut
     * @param int        $type
     * @param int        $timestamp
     *
     * @return FxRates
     */
    public function getRate(Currencies $currencyIn, Currencies $currencyOut, int $type, int $timestamp);

    /**
     * @return int
     */
    public function getSource();
}
<?php

namespace ffsoft\fx\drivers;

use common\models\Currencies;
use common\models\enums\RateTypes;
use darkdrim\simplehtmldom\SimpleHTMLDom as SHD;
use ffsoft\fx\enums\FxRatesSource;
use ffsoft\fx\models\FxRates;
use Yii;

/**
 * Class Kurzy
 *
 * @package ffsoft\fx\drivers
 */
class Kurzy implements RatesInterface
{
    /** Currency code */
    const DEFAULT_CURRENCY = 'CZK';
    /** @var string */
    private $url = 'https://www.kurzy.cz/kurzy-men/aktualni/';

    /**
     * @param Currencies $currencyIn
     * @param Currencies $currencyOut
     * @param int        $type
     * @param int        $timestamp
     *
     * @return FxRates|object|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getRate(Currencies $currencyIn, Currencies $currencyOut, int $type, int $timestamp)
    {
        $date = date('d.m.Y-H.i', $timestamp);
        $key = [$date, __METHOD__];
        $cache = Yii::$app->cache->get($key);

        if ($cache === false) {
            $cache = $this->parseData();
            Yii::$app->cache->set($key, $cache, 60);
        }

        $rate = $this->fetchData($cache, $currencyIn->getName(), $currencyOut->getName(), $type);

        if (!empty($rate)) {
            $fxRate = Yii::createObject([
                'class'           => FxRates::class,
                'currency_id_in'  => $currencyIn->getId(),
                'currency_id_out' => $currencyOut->getId(),
                'source'          => $this->getSource(),
                'rate'            => $rate['rate'],
                'type'            => $rate['type'],
                'created_at'      => $rate['created_at'],
            ]);

            return $fxRate;
        } else {
            return null;
        }
    }

    /**
     * @return int
     */
    public function getSource()
    {
        return FxRatesSource::KURZY;
    }

    /**
     * @param array  $data
     * @param string $currency_in
     * @param string $currency_out
     * @param int    $type
     *
     * @return array|null
     */
    private function fetchData(array $data, string $currency_in, string $currency_out, int $type)
    {
        foreach ($data as $rate) {
            if ($currency_in == $rate['currency_in']
                && $currency_out == $rate['currency_out']
                && $type == $rate['type']
            ) {
                return $rate;
            }
        }

        return null;
    }

    /**
     * TODO: PHP7.1 Rewrite using paquettg/php-html-parser after upgrade
     *
     * @return array|bool
     */
    private function parseData()
    {
        $data = [];
        $response = SHD::file_get_html($this->url);

        if ($response) {
            /** Parse CZK pairs */
            $table = $response->find('table', 0);
            $date = explode(' ', $table->prev_sibling()->prev_sibling()->plaintext);
            $timestamp = strtotime($date[3] . '' . $date[4]);

            foreach ($table->children as $tr) {
                if (!($tr->has_child() && $tr->children(2)->has_child())) {
                    continue;
                }

                $currency = $tr->children(2)->first_child()->plaintext;
                if (!empty($currency)) {
                    $rate = floatval($tr->children(8)->first_child()->plaintext);
                    $amount = preg_replace('/\D/', '', $tr->children(3)->plaintext);
                    if (empty($amount)) {
                        continue;
                    }

                    $data[] = [
                        'currency_in'  => $currency,
                        'currency_out' => self::DEFAULT_CURRENCY,
                        'rate'         => round($rate / $amount, 8),
                        'type'         => RateTypes::SELL,
                        'created_at'   => $timestamp,
                    ];

                    $data[] = [
                        'currency_in'  => self::DEFAULT_CURRENCY,
                        'currency_out' => $currency,
                        'rate'         => round($rate / $amount, 8),
                        'type'         => RateTypes::BUY,
                        'created_at'   => $timestamp,
                    ];
                }
            }

            /** Parse Foreign pairs */
            $table = $response->find('table', 2);
            $date = explode(' ', $table->prev_sibling()->plaintext);
            $timestamp = strtotime($date[3] . '' . $date[4]);

            foreach ($table->children as $tr) {
                if (!($tr->has_child() && $tr->children(2)->has_child())) {
                    continue;
                }

                $currency_pair = substr($tr->children(0)->first_child()->plaintext, 1);
                if (!empty($currency_pair)) {
                    $currency_pair = explode('/', $currency_pair);
                    $rate = floatval($tr->children(8)->first_child()->plaintext);
                    $rate = round($rate, 8);

                    $currencyIn = $currency_pair[0];
                    $currencyOut = $currency_pair[1];

                    $data[] = [
                        'currency_in'  => $currencyIn,
                        'currency_out' => $currencyOut,
                        'rate'         => $rate,
                        'type'         => RateTypes::SELL,
                        'created_at'   => $timestamp,
                    ];

                    $data[] = [
                        'currency_in'  => $currencyOut,
                        'currency_out' => $currencyIn,
                        'rate'         => $rate,
                        'type'         => RateTypes::BUY,
                        'created_at'   => $timestamp,
                    ];
                }
            }

            return $data;
        } else {
            return false;
        }
    }
}
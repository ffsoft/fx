<?php

namespace ffsoft\fx;

/**
 * Class Module
 *
 * @package ffsoft\fx
 */
class Module extends \yii\base\Module
{
    /**
     * @var string
     */
    public $controllerNamespace = 'ffsoft\fx\controllers';
}